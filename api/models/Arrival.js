/**
 * Arrival.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    email: {
      type: "string",
      required: true
    },
    name: {
      type: "string",
      required: true
    },
    date: {
      type: "date",
      required: true
    },
    paxCount: {
      type: "number"
    },
    alreadySent: {
      type: "boolean",
      default: false
    }
  },

};

