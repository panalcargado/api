module.exports = {


  friendlyName: 'Create new arrival',


  description: 'Create arrival.',


  inputs: {
    email: {
      type: "string",
      required: true
    },
    name: {
      type: "string",
      required: true
    },
    date: {
      type: "date",
      required: true
    },
    paxCount: {
      type: "number"
    }
  },


  exits: {
    success: {
      statusCode: 200,
      description: 'Arrival has been created successfully',
    },
    unauthorized: {
      statusCode: 401,
      description: 'Unauthorized request',
    }
  },


  fn: async function (inputs, exits) {
    var newArrival = Arrival.create({
      email: inputs.email,
      name: inputs.name,
      date: inputs.date,
      paxCount: inputs.paxCount
    }).fetch();

    if (newArrival.length === 0) { return exits.unauthorized({ message: "Unauthorized request", statusCode: 401 }) }

    var responseData = {
      newArrival,
      message: 'Arrival has been created successfully',
      statusCode: 200
    }

    return exits.success(responseData);

  }


};
